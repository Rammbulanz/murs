# murs

My Universal REST Service

Collection of functions to be evaluated in node context for usage in plain frontend projects.

Also a helper to avoid redundance functions in my apps.

## Route: /api/audio

```
# Description: Send a audio file to murs service. Murs service will try to parse the metadata out of the file.
/api/audio/metadata
Content-Type: audio/*
Body: File
```