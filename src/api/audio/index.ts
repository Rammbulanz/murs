import * as KoaRouter from "koa-router";
import { combineEndPoints, acceptedType } from "../../helpers";

export const router = new KoaRouter({
    prefix: "/audio"
});

import { parseMetadata, parseCover } from './metadata'
router.post('/metadata', parseMetadata);
router.post('/metadata/cover', parseCover);