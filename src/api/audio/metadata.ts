import { Context } from "koa";
import * as mm from 'music-metadata'
import { IPicture } from "music-metadata/lib/type";
import { Nullable } from "../../types/Nullable";

/**
 * Extract the metadata from an audio stream
 */
export async function parseMetadata(ctx: Context): Promise<Nullable<mm.ICommonTagsResult>> {

    if (ctx.request.type.startsWith("audio/")) {

        const metadata = await mm.parseStream(ctx.req, ctx.request.type);

        ctx.status = 200;
        ctx.body = metadata.common;
        ctx.type = "application/json";

        return ctx.body;

    } else {

        ctx.throw("Unsupported media type", 503);

    }

}

/**
 * Extract the picture from an audio stream
 */
export async function parseCover(ctx: Context): Promise<Nullable<IPicture>> {

    if (ctx.request.type.startsWith("audio/")) {

        const metadata = await parseMetadata(ctx);

        if (metadata) {

            const pictures = metadata.picture;

            if (pictures.length > 0) {

                const cover = pictures[0];

                ctx.body = cover.data;
                ctx.type = cover.format;

                return cover;

            } else {

                ctx.throw(404, "No pictures contained in file");

            }

        }

    }

}