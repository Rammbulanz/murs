import * as KoaRouter from 'koa-router'

export const router = new KoaRouter({
    prefix: '/api/v1'
});

import { router as audio } from './audio'
router.use(audio.routes(), audio.allowedMethods());

import { router as geometry } from './geometry'
router.use(geometry.routes(), geometry.allowedMethods());