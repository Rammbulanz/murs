import { Context } from "koa";
import { IMiddleware } from "koa-router";

export const combineEndPoints = (...endPoints: Array<(ctx: Context) => Promise<any>>): IMiddleware => async (ctx: Context) => {
    for (const mw of endPoints) {
        await mw(ctx);
    }
}

export const acceptedType = (pattern: RegExp): IMiddleware => async (ctx: Context, next: () => Promise<any>) => {
    if (pattern.test(ctx.request.type)) {
        await next();
    } else {
        ctx.throw("Unsupported media type", 415);
    }
}